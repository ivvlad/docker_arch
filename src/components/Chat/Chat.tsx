/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';

import styles from './chat.module.css';
import ChatHeader from './ChatHeader/ChatHeader';
import appConfig from '../../appConfig';
import ChatBody from './ChatBody/ChatBody';
import ChatInput from './ChatInput/ChatInput';
import Preloader from '../Preloader/Preloader';

import { errorHandler, getUserFromMessages, getUsersCount, uniqueId } from '../../helpers/chat';
import { IChatProps, IChatContainerProps, IMessage, IMessageResponse, IUser } from '../../interfaces';

const Chat:React.FC<IChatProps> = ({ 
  messages, 
  handleAddMessage, 
  handleLike, 
  editText, 
  handleSetEditMode, 
  handleEdit, 
  handleDelete 
}) => {
  const user: IUser = getUserFromMessages(messages, appConfig.activeUserId || '');
  const usersCount:number = getUsersCount(messages);
  const lastMessageTime = messages.length && messages[messages.length - 1].createdAt || '';
   
  return (
    <div className={`${styles.chatWrapper} chat`}>
      <ChatHeader 
        chatName={appConfig.chatName || ''} 
        user={user} 
        userCount={usersCount} 
        messagesCount={messages.length} 
        lastMessageTime={lastMessageTime} 
      />
      <ChatBody 
        messages={messages} 
        handleAddMessage={() => null} 
        handleLike={handleLike} 
        editText=""
        handleSetEditMode={handleSetEditMode} 
        handleEdit={() => null}
        handleDelete={handleDelete} />
      <ChatInput handleAddMessage={handleAddMessage} editText={editText} handleEdit={handleEdit} />
    </div>
  );
};

const ChatContainer: React.FC<IChatContainerProps> = ({ url }) => {
  const activeUserId = 'my-id';

  const [loader, setLoader] = useState(false);
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [editText, setEditText] = useState('');
  const [messageId, setMessageId] = useState('');

  sessionStorage.setItem('id', activeUserId);
  
  useEffect(() => {
    setLoader(true);
    
    fetch(url)
      .then(async (res:Response) => {
        if (res.ok && res) {
          const json = await res.json();
          const updatedMessages:IMessage[] = json.map((messageData:IMessageResponse) => ({ ...messageData, likes: 0})); 

          setMessages(updatedMessages);
        } else {
          console.warn('Message request error.');
        }

        setLoader(false);
      })
      .catch(err => {
        setLoader(false);
        errorHandler(err);
      });
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleAddMessage = (text:string):void => {
    if (text) {
      const userData = messages.find((messageData) => messageData.userId === activeUserId);
      const message:IMessage = {
        avatar: userData?.avatar || '',
        createdAt: new Date().toISOString(),
        editedAt: '',
        id: uniqueId(),
        likes: 0,
        text,
        user: userData?.user || '',
        userId: activeUserId || '',
      };

      setMessages((prevMessages) => ([ ...prevMessages, message ]));
    }
  };

  const handleLike = (id:string):void => {
    const updatedMessages:IMessage[] = [ ...messages.map(message => {
      if (message.id === id) {
        return { ...message, likes: message.likes + 1 };
      }

      return message;
    })] || [];

    setMessages(updatedMessages);
  };

  const handleSetEditMode = (id:string):void => {
    const message = messages.find((messageData) => messageData.id === id);

    if (message) {
      setEditText(message.text);
      setMessageId(id);
    }
  };

  const handleEdit = (text:string):void => {
    const updatedMessages:IMessage[] = [ ...messages.map(message => {
      if (message.id === messageId) {
        return { ...message, text, editedAt: new Date().toISOString(), };
      }

      return message;
    })] || [];

    setMessages(updatedMessages);
    setMessageId('');
    setEditText('');
  };

  const handleDelete = (id:string):void => {
    const updatedMessages:IMessage[] = messages.filter(message => message.id !== id);

    setMessages(updatedMessages);
  };

  return (
    <>
      <Preloader visibility={loader} />
      <Chat 
        messages={messages} 
        handleAddMessage={handleAddMessage} 
        handleLike={handleLike} 
        editText={editText}
        handleSetEditMode={handleSetEditMode}
        handleEdit={handleEdit} 
        handleDelete={handleDelete} 
      />
    </>
  );
};

export default ChatContainer;