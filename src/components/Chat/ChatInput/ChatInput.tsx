import React, { useState, useEffect } from 'react';

import style from './chatInput.module.css';

import { IChatInput } from '../../../interfaces';

const ChatInput: React.FC<IChatInput> = ({ handleAddMessage, editText, handleEdit }) => {
  const [text, setText] = useState('');

  useEffect(() => {
    if (editText) {
      setText(editText);
    }
  }, [editText]);

  const handleSendMessage = () => {
    if (editText) {
      handleEdit(text);
      setText('');
    } else {
      handleAddMessage(text);
      setText('');
    }
  };

  return (
    <div className={`${style.inputWrapper} message-input`}>
      <textarea 
        className={`${style.textArea} message-input-text`}
        value={text} 
        onChange={(ev: React.FormEvent<HTMLTextAreaElement>) => setText(ev.currentTarget.value)} />
      <button className={`${style.submitButton} message-input-button`} type="button" onClick={handleSendMessage}>Отправить</button>
    </div>
  );
};

export default ChatInput;
