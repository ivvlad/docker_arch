import React from 'react';

import styles from './chatBody.module.css';
import MessagesDateContainer from './MessagesDateContainer/MessagesDateContainer';

import { IChatProps } from '../../../interfaces';
import { filterMessagesByDate } from '../../../helpers/chat';

// eslint-disable-next-line
const ChatBody: React.FC<IChatProps> = ({ messages, handleLike, handleSetEditMode, handleDelete }) => {
  const filteredMessages = filterMessagesByDate(messages);

  return (
    <main className={`${styles.messages} message-list`}>
      {filteredMessages.map(((data) => (
        <MessagesDateContainer 
          key={data[1][0].id} 
          date={data[0]} 
          messages={data[1]} 
          handleLike={handleLike} 
          handleSetEditMode={handleSetEditMode} 
          handleDelete={handleDelete} 
        />
      )))}
    </main>
  );
};

export default ChatBody;
