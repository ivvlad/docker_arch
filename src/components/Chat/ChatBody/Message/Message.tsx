import React, { useState } from 'react';

import styles from './message.module.css';
import likeIcon from './assets/like.svg';
import optionIcon from './assets/more-options.svg';

import { IMessageContainer } from '../../../../interfaces';
import { formattedTime } from '../../../../helpers/chat';

const Message: React.FC<IMessageContainer> = ({ message, handleLike, handleSetEditMode, handleDelete }) => {
  const [optionsActive, setOptionsActive] = useState(false);

  const activeUserId = sessionStorage.getItem('id');
  const isUserMessage = activeUserId === message.userId;

  return (
    <li className={isUserMessage ? `${styles.ownMessage} own-message` : `${styles.message} message`}>
      {!isUserMessage && <img className={`${styles.avatar} message-user-avatar`} src={message.avatar} alt={`Аватар пользователя ${message.user}`} /> }
      <div className={styles.messageWrapper}>
        <div className={styles.messageBody}>
          <p className={`${styles.messageText} message-text`}>
            {message.text}
          </p>
          <span className={`${styles.messageTime} message-time`}>{formattedTime(message.createdAt)}</span>
          {!isUserMessage && (
            <button className={`${styles.likes} ${message.likes ? 'message-liked' : 'message-like'}`} type="button" onClick={() => handleLike(message.id)}>
              <span className={styles.likesCount}>{message.likes}</span>
              <img src={likeIcon} alt="понравилось" />
              <span className="visually-hidden">Лайк</span>
            </button>
          )}
        </div>
        {!isUserMessage && <h5 className={`${styles.userName} message-user-name`}>{message.user}</h5> }
      </div>
      {isUserMessage && (
        <>
          <button type="button" className={styles.optionButton} onClick={() => setOptionsActive(!optionsActive)}>
            <img src={optionIcon} alt="Опции" />
            <span className="visually-hidden">options</span>
          </button>
          <ul className={optionsActive ? styles.options : styles.optionDisabled}>
            <li className={styles.option}>
              <button className={`${styles.messageEdit} message-edit`} type="button" onClick={() => {
                handleSetEditMode(message.id);
                setOptionsActive(false);
              }}>
                Изменить
              </button>
            </li>
            <li className={styles.option}>
              <button className={`${styles.messageDelete} message-delete`} type="button" onClick={() => handleDelete(message.id)}>
                Удалить
              </button>
            </li>
          </ul>
        </>
      )}
    </li>
  );
};

export default Message;