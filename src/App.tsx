import React from 'react';
import ChatContainer from './components/Chat/Chat';

const App: React.FC = () => (
  <ChatContainer url="https://edikdolynskyi.github.io/react_sources/messages.json" />
);

export default App;
