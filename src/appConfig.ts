const appConfig = {
  chatName: process.env.REACT_APP_CHAT_NAME,
  activeUserId: process.env.REACT_APP_ACTIVE_USER_ID,
};

export default appConfig;