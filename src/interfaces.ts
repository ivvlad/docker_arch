export interface IChatContainerProps {
  url: string,
}

export interface IChatProps {
  messages: IMessage[],
  handleAddMessage(test:string):void,
  handleLike(id:string):void,
  editText:string,
  handleSetEditMode(id:string):void,
  handleEdit(text:string):void,
  handleDelete(id:string):void,
}

export interface IChatHeader {
  chatName: string,
  user: IUser,
  userCount: number, 
  messagesCount: number, 
  lastMessageTime: string,
}

export interface IMessageDateContainer {
  date: string,
  messages: IMessage[],
  handleLike(id:string):void,
  handleSetEditMode(id:string):void,
  handleDelete(id:string):void,
}

export interface IMessageContainer {
  key: string,
  message: IMessage,
  handleLike(id:string):void,
  handleSetEditMode(id:string):void,
  handleDelete(id:string):void,
}

export interface IChatInput {
  handleAddMessage(text:string):void,
  editText:string,
  handleEdit(text:string):void
}

export interface IPreloader {
  visibility:boolean,
}

export interface IMessageResponse {
  id: string,
  userId: string,
  avatar: string,
  user: string,
  text: string,
  createdAt: string,
  editedAt: string,
}

export interface IMessage {
  id: string,
  userId: string,
  avatar: string,
  user: string,
  text: string,
  createdAt: string,
  editedAt: string,
  likes: number,
}

export interface IUser {
  userId: string,
  avatar: string,
  name: string,
}

export interface IFilteredMessages {
  date: string,
  messages: IMessage[],
}