FROM node:alpine

COPY . .
COPY package*.json .
RUN yarn install
RUN yarn build

CMD ["yarn", "serve", "-s", "build"]